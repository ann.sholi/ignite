import React, { useEffect } from 'react';
// Styling and Animation
import styled from 'styled-components';
import { motion, AnimatePresence, AnimateSharedLayout } from 'framer-motion';
import { useLocation } from 'react-router-dom';
// Redux
import { useDispatch, useSelector } from 'react-redux';

import GameDetails from '../components/GameDetails';
import { loadGames } from '../actions/gamesAction';
import { loadDetail } from '../actions/detailAction';
// Components
import GameSection from '../components/GameSection';
import Loader from '../components/Loader';

const Home = () => {
  // get the current location
  const location = useLocation();
  const pathId = location.pathname.split('/')[2];
  // FETCH GAMES
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadGames());
  }, [dispatch]);

  // Get that data back
  const {
    popular, newGames, upcoming, searched, isFetching,
  } = useSelector((state) => state.games);

  const loadDetailHandler = (id) => {
    // eslint-disable-next-line no-undef
    document.body.style.overflow = 'hidden';
    dispatch(loadDetail(id.toString()));
  };

  if (isFetching) return <Loader />;

  return (
    <GameList>
      <AnimateSharedLayout type="crossfade">
        <AnimatePresence>
          { pathId && <GameDetails pathId={pathId} /> }
        </AnimatePresence>
        {searched.length ? (
          <div className="searched">
            <GameSection title="Searched Games" gamesData={searched} onGameDetailsHandler={loadDetailHandler} />
          </div>
        ) : (
          ''
        )}
        <GameSection title="Upcoming Games" gamesData={upcoming} onGameDetailsHandler={loadDetailHandler} />
        <GameSection title="Popular Games" gamesData={popular} onGameDetailsHandler={loadDetailHandler} />
        <GameSection title="New Games" gamesData={newGames} onGameDetailsHandler={loadDetailHandler} />
      </AnimateSharedLayout>
    </GameList>
  );
};

const GameList = styled(motion.div)`
  padding: 0 5rem;
  h2 {
    padding: 5rem 0;
  }
`;

export default Home;
