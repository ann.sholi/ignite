import axios from 'axios';

import {
  popularGamesURL, upcomingGamesURL, newGamesURL, searchedGameURL,
} from '../api';

export const FETCH_GAMES = 'FETCH_GAMES';
export const FETCH_GAMES_SUCCESS = 'FETCH_GAMES_SUCCESS';
export const FETCH_GAMES_ERROR = 'FETCH_GAMES_ERROR';
export const FETCH_SEARCHED = 'FETCH_SEARCHED';

// Action Creator

export const loadGames = () => async (dispatch) => {
  dispatch({ type: FETCH_GAMES });
  // FETCH AXIOS
  try {
    const popularData = await axios.get(popularGamesURL());
    const upcomingData = await axios.get(upcomingGamesURL());
    const newGamesData = await axios.get(newGamesURL());

    dispatch({
      type: FETCH_GAMES_SUCCESS,
      payload: {
        popular: popularData.data.results,
        upcoming: upcomingData.data.results,
        newGames: newGamesData.data.results,
      },
    });
  } catch (e) {
    dispatch({
      type: FETCH_GAMES_ERROR,
      payload: e,
    });
  }
};

export const fetchSearch = (gameName) => async (dispatch) => {
  try {
    const searchGames = await axios.get(searchedGameURL(gameName));

    dispatch({
      type: FETCH_SEARCHED,
      payload: {
        searched: searchGames.data.results,
      },
    });
  } catch (e) {
    dispatch({ type: 'ERROR', payload: e });
  }
};
