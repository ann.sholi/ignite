import axios from 'axios';

import { gameDetailsURL, gameScreenshotURL } from '../api';

export const LOADING_DETAIL = 'LOADING_DETAIL';
export const GET_DETAIL = 'GET_DETAIL';
export const FETCH_DETAILS_ERROR = 'FETCH_DETAILS_ERROR';
// Action Creator

// eslint-disable-next-line import/prefer-default-export
export const loadDetail = (id) => async (dispatch) => {
  dispatch({ type: LOADING_DETAIL });

  try {
    const detailData = await axios.get(gameDetailsURL(id));
    const screenShotData = await axios.get(gameScreenshotURL(id));

    dispatch({
      type: GET_DETAIL,
      payload: {
        game: detailData.data,
        screen: screenShotData.data,
      },
    });
  } catch (e) {
    dispatch({ type: FETCH_DETAILS_ERROR, payload: e });
  }
};
