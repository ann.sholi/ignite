export default [
  {
    name: 'Far Cry 6',
    released: '2021-02-18',
    id: 463723,
    image: 'https://media.rawg.io/media/games/5dd/5dd4d2dd986d2826800bc37fff64aa4f.jpg',
    key: 463723,
  },
  {
    name: 'Hitman 3 (2021)',
    released: '2021-01-20',
    id: 452645,
    image: 'https://media.rawg.io/media/games/126/126fbd5ceacddc6ad16fc96e50b1265b.jpg',
    key: 452645,
  },
];
