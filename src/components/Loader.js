import React from 'react';
import styled from 'styled-components';
import loader from '../img/loader.gif';

const Loader = () => (
  <Spiner>
    <img src={loader} alt="" />
  </Spiner>
);

const Spiner = styled.div`
  img{
    height: 250px;
    margin: 100px auto 0;
    width: 250px;
  }
`;

export default Loader;
