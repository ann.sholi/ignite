import React from 'react';
import { shallow } from 'enzyme';

import GameSection from '../GameSection';
import testData from '../../mockData/testData';
import Game from '../Game';

const propsData = {
  title: 'Test game section',
  gamesData: testData,
  onGameDetailsHandler: null,
};

const setUp = (props = propsData) => {
  const component = shallow(<GameSection {...props} />);
  return component;
};

describe('Game Section component', () => {
  let component;
  beforeEach(() => {
    component = setUp();
  });

  it('should render without errors', () => {
    const wrapper = component.find(`[data-test='game-section']`);
    expect(wrapper.length).toBe(1);
  });

  it('should render proper headline for the games section', () => {
    const headline = component.find('h2');
    expect(headline.text()).toBe(propsData.title);
  });

  it('should render game components', () => {
    expect(component.find(Game)).toHaveLength(testData.length);
  });
});
