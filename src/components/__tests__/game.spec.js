import React from 'react';
import { shallow } from 'enzyme';
import checkPropTypes from 'check-prop-types';
import { motion } from 'framer-motion';

import Game from '../Game';
import gameData from '../../mockData/testData';

const setUp = (props = gameData[0], onClickHandler = null) => {
  const component = shallow(<Game {...props} onGameDetailsHandler={onClickHandler} />);
  return component;
};

describe('Game component', () => {
  let component;
  beforeEach(() => {
    component = setUp();
  });

  it('The game should not render in case empty data', () => {
    component = setUp({});
    const wrapper = component.find(`[data-test='game-card']`);
    expect(wrapper.length).toBe(0);
  });

  it('should render game headline', () => {
    const headline = component.find(motion.h3);
    expect(headline.text()).toBe(gameData[0].name);
  });

  it('should render game released info', () => {
    const released = component.find('p');
    expect(released.text()).toBe(gameData[0].released);
  });

  it('should render game image', () => {
    const image = component.find(motion.img);

    expect(image.length).toBe(1);
    expect(image.prop('alt')).toEqual(gameData[0].name);
    expect(image.prop('src')).toEqual(gameData[0].image);
  });

  it('should handle click event', () => {
    const f = jest.fn();
    component = setUp(gameData[0], f);
    const wrapper = component.find(`[data-test='game-card']`);
    wrapper.simulate('click');

    expect(f).toHaveBeenCalledTimes(1);
    expect(f).toHaveBeenCalledWith(gameData[0].id);
  });

  describe('Checking PropTypes', () => {
    it('should not throw a warning', () => {
      const expectedProps = {
        name: 'Test Name',
        released: 'Test Released',
        image: 'Test Image',
        id: 1,
        onGameDetailsHandler: jest.fn(),
      };
      const propsErr = checkPropTypes(Game.propTypes, expectedProps, 'props', Game.name);
      expect(propsErr).toBeUndefined();
    });
  });
});
