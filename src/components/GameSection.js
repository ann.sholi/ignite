import React from 'react';
import PropTypes from 'prop-types';
import { motion } from 'framer-motion';
// Styling and Animation
import styled from 'styled-components';

import Game from './Game';

const GameSection = ({ title, gamesData, onGameDetailsHandler }) => (
  <div data-test="game-section">
    <h2>{ title }</h2>
    <Games>
      {gamesData.map((game) => (
        <Game
          name={game.name}
          released={game.released}
          id={game.id}
          image={game.background_image}
          key={game.id}
          onGameDetailsHandler={onGameDetailsHandler}
        />
      ))}
    </Games>
  </div>
);

GameSection.propTypes = {
  title: PropTypes.string,
  gamesData: PropTypes.arrayOf(PropTypes.object),
  onGameDetailsHandler: PropTypes.func,
};

const Games = styled(motion.div)`
  min-height: 80vh;
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(320px, 1fr));
  grid-column-gap: 3rem;
  grid-row-gap: 5rem;
`;

export default GameSection;
