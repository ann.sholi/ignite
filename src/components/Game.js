import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
// Styling & Animations
import styled from 'styled-components';
import { motion } from 'framer-motion';

const Game = ({
  name, released, image, id, onGameDetailsHandler,
}) => (
  <>
    {id && (
      <StyledGame layoutId={id?.toString()} onClick={() => onGameDetailsHandler(id)} data-test="game-card">
        <Link to={`/game/${id}`}>
          <motion.h3 layoutId={`title ${id}`}>{name}</motion.h3>
          <p>{released}</p>
          <motion.img layoutId={`image ${id}`} src={image} alt={name} />
        </Link>
      </StyledGame>
    )}
  </>
);

const StyledGame = styled(motion.div)`
  min-height: 30vh;
  box-shadow: 0 5px 20px rgba(0, 0, 0, .2);
  text-align: center;
  border-radius: 1rem;
  cursor: pointer;
  overflow: hidden;
  img{
    width: 100%;
    height: 40vh;
    object-fit: cover;
  }
`;

Game.propTypes = {
  name: PropTypes.string,
  released: PropTypes.string,
  image: PropTypes.string,
  id: PropTypes.number,
  onGameDetailsHandler: PropTypes.func,
};

export default Game;
