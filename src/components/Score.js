import React from 'react';
import PropTypes from 'prop-types';

// Star images
import starEmpty from '../img/star-empty.png';
import starFull from '../img/star-full.png';

const Score = ({ rating }) => {
  const stars = [];
  const ratingStars = Math.floor(rating);

  for (let i = 1; i <= 5; i += 1) {
    if (i <= ratingStars) {
      stars.push(<img alt="star" key={i} src={starFull} />);
    } else {
      stars.push(<img alt="star" key={i} src={starEmpty} />);
    }
  }
  return (
    <>
      {stars}
    </>
  );
};

Score.propTypes = {
  rating: PropTypes.string,
};

export default Score;
