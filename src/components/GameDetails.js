import React from 'react';
import PropTypes from 'prop-types';
// Styling & Animation
import styled from 'styled-components';
import { motion } from 'framer-motion';
// Redux
import { useSelector } from 'react-redux';

import { useHistory } from 'react-router-dom';

import Score from './Score';
// Importing Images
import playstation from '../img/playstation.svg';
import steam from '../img/steam.svg';
import xbox from '../img/xbox.svg';
import nintendo from '../img/nintendo.svg';
import apple from '../img/apple.svg';
import gamepad from '../img/gamepad.svg';

const GameDetails = ({ pathId }) => {
  const history = useHistory();
  // Exit Detail
  const exitDetailHandler = (e) => {
    const element = e.target;
    if (element.classList.contains('shadow')) {
      // eslint-disable-next-line no-undef
      document.body.style.overflow = 'auto';
      history.push('/');
    }
  };

  // Get Platform Images
  const getPlatform = (platform) => {
    switch (platform) {
      case 'PlayStation 4':
        return playstation;
      case 'PlayStation 5':
        return playstation;
      case 'Xbox Series S/X':
        return xbox;
      case 'Xbox S':
        return xbox;
      case 'Xbox One':
        return xbox;
      case 'PC':
        return steam;
      case 'Nintendo Switch':
        return nintendo;
      case 'iOS':
        return apple;
      default:
        return gamepad;
    }
  };

  // Data
  const { screen, game, isLoading } = useSelector((state) => state.detail);

  return (
    <>
      {!isLoading && (
      <CardShadow className="shadow" onClick={exitDetailHandler}>
        <Detail layoutId={pathId}>
          <Stats>
            <div className="rating">
              <motion.h3 layoutId={`title ${pathId}`}>{game.name}</motion.h3>
              <p>
                {`Rating: ${game.rating}`}
              </p>
              <Score rating={game.rating} />
            </div>
            <Info>
              <h3>Platfoms</h3>
              <Platfoms>
                {game.platforms.map((data) => (
                  <img key={data.platform.id} src={getPlatform(data.platform.name)} alt={data.platform.name} />
                ))}
              </Platfoms>
            </Info>
          </Stats>

          <Media>
            <motion.img layoutId={`image ${pathId}`} src={game.background_image} alt={game.background_image} />
          </Media>

          <Description>
            <p>{game.description_raw}</p>
          </Description>
          <div className="gallery">
            {screen.results.map((sc) => (
              <img src={sc.image} key={sc.id} alt={sc.image} />
            ))}
          </div>
        </Detail>
      </CardShadow>
      )}
    </>
  );
};

const CardShadow = styled(motion.div)`
  width: 100%;
  min-height: 100vh;
  overflow-y: scroll;
  background: rgba(0, 0, 0, .5);
  position: fixed;
  top: 0;
  left: 0;
  z-index: 99;
  
  &::-webkit-scrollbar {
      width: .5rem;
    }
  &::-webkit-scrollbar-thumb {
    background-color: #ff7676;
  }
  &::-webkit-scrollbar-track {
    background: white;
  }
`;

const Detail = styled(motion.div)`
  width: 70%;
  border-radius: 1rem;
  padding: 2rem 5rem;
  background: white;
  position: absolute;
  left: 15%;
  top: 10%;
  color: black;
  z-index: 100;
  img {
    width: 100%;
  }
`;

const Stats = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: space-between;
  img {
    width: 1rem;
    height: 1rem;
    display: inline;
  }
`;

const Info = styled(motion.div)`
  text-align: center;
  h3 {
    text-align: right;
  }
`;

const Platfoms = styled(motion.div)`
  display: flex;
  justify-content: space-evenly;
  img {
    margin-left: 1rem;
    width: 1.5rem;
    height: 1.5rem;
  }
`;

const Media = styled(motion.div)`
  margin-top: 5rem;
  img {
    width: 100%;
  }
`;

const Description = styled(motion.div)`
  margin: 5rem 0;
`;

GameDetails.propTypes = {
  pathId: PropTypes.string,
};

export default GameDetails;
