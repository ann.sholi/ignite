import gamesReducer from './gamesReducer';
import {
  FETCH_GAMES,
  FETCH_GAMES_SUCCESS,
  FETCH_GAMES_ERROR,
} from '../actions/gamesAction';

describe("gamesReducer()", () => {
  it('returns init state if action is not found', () => {
    expect(gamesReducer(undefined, {})).toEqual({
      popular: [],
      newGames: [],
      upcoming: [],
      searched: [],
      isFetching: true,
    });
  });

  it('Should return new state if receiving type', () => {
    const games = {
      popular: [],
      upcoming: [],
      newGames: [],
      searched: [],
      isFetching: true,
    };
    const newState = gamesReducer(undefined, { type: FETCH_GAMES });
    expect(newState).toEqual(games);
  });

  it('Should return new state if receiving type', () => {
    const games = {
      popular: [{ title: 'Test title' }],
      upcoming: [{ title: 'Test title-2' }],
      newGames: [{ title: 'Test title-3' }],
      searched: [],
      isFetching: false,
    };
    const newState = gamesReducer(undefined, {
      type: FETCH_GAMES_SUCCESS,
      payload: games,
    });
    expect(newState).toEqual(games);
  });

  it('Should return new state if receiving type', () => {
    const games = {
      popular: [],
      upcoming: [],
      newGames: [],
      searched: [],
      isFetching: false,
    };
    const newState = gamesReducer(undefined, { type: FETCH_GAMES_ERROR });
    expect(newState).toEqual(games);
  });
});
