import { combineReducers } from 'redux';

import gamesReducer from './gamesReducer';
import detailReduser from './detailReducer';

const rootReducer = combineReducers({
  games: gamesReducer,
  detail: detailReduser,
});

export default rootReducer;
