import {
  FETCH_GAMES,
  FETCH_GAMES_SUCCESS,
  FETCH_GAMES_ERROR,
  FETCH_SEARCHED,
} from '../actions/gamesAction';

const initState = {
  popular: [],
  newGames: [],
  upcoming: [],
  searched: [],
  isFetching: true,
};

const gamesReducer = (state = initState, action) => {
  switch (action.type) {
    case FETCH_GAMES:
      return {
        ...state,
        isFetching: true,
      };
    case FETCH_GAMES_SUCCESS:
      return {
        ...state,
        popular: action.payload.popular,
        upcoming: action.payload.upcoming,
        newGames: action.payload.newGames,
        isFetching: false,
      };
    case FETCH_GAMES_ERROR:
      return {
        ...state,
        isFetching: false,
      };
    case FETCH_SEARCHED:
      return {
        ...state,
        searched: action.payload.searched,
      };
    case 'CLEAR_SEARCHED':
      return {
        ...state,
        searched: [],
      };
    default:
      return { ...state };
  }
};

export default gamesReducer;
