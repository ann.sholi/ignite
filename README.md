# About app
This application is game portal which is based on React & open API [Rawg.io](https://rawg.io/). 

It covered by unit tests.

# Project start

1. `git clone https://gitlab.com/ann.sholi/ignite.git` 

2. `cd ignine`

3. `docker-compose up -d --build` to build and start application

4. Open [http://localhost:3001](http://localhost:3001) to view it in the browser.


** Run `docker-compose down` to stop running containers


# Running unit tests

Run `docker-compose run --rm ignite npm test` to execute tests

